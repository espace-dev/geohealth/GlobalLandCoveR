
<!-- README.md is generated from README.Rmd. Please edit that file -->

# GlobalLandCoveR

<!-- badges: start -->

[![Project Status: WIP - Initial development is in progress, but there
has not yet been a stable, usable release suitable for the
public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)
<!-- badges: end -->

The goal of `GlobalLandCover` is to make easier for researcher to access
Landcover data from the Department of Earth System Science at Tsinghua
University.

<http://data.ess.tsinghua.edu.cn/>

Data are under CC-BY-4.0 license
