---
title: "flat_globallandcover.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(rvest)
library(stringr)
library(sf)
library(dplyr)
library(cli)
library(httr)
```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

# Download LandCover Tiles

The `downloadLandcover()` function allows to retrieve the landcover data for a selected area.
It takes as input an sf object and a download directory.

```{r function-downloadLandcover}
#' Download Landcover tiles
#'
#' @param aoi sf object EPSG:4326
#' @param download_directory download directory
#'
#' @return nothing
#' @importFrom dplyr filter
#' @importFrom sf st_intersects
#' @importFrom cli cli_progress_bar
#' @importFrom httr GET write_disk timeout
#' @export
#'
#' @examples
downloadLandcover <- function(aoi, download_directory){
  spatialSubset <- subset(index, st_intersects(geometry, aoi, sparse = FALSE))
  cli_progress_bar("Download tiles", total = nrow(spatialSubset))
  for(i in 1:nrow(spatialSubset)){
    cli_progress_update()
    GET(url = paste0("http://data.ess.tsinghua.edu.cn/data/fromglc10_2017v01/", spatialSubset[i,]$filename),
        write_disk(paste(download_directory, spatialSubset[i,]$filename), overwrite = TRUE) ,
        timeout(300L),
        quiet = TRUE)
  }
  cli_progress_done()
}
```

```{r examples-downloadLandcover, eval=FALSE}
#' \dontrun{
library(nominatimlite)
aoi <- geo_lite_sf("Mondulkiri", points_only = FALSE) %>%
  st_transform(crs=4326)

downloadLandcover(aoi = aoi, index=getTilesIndex(), download_directory = tempdir())
#' }
```

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_globallandcover.Rmd", vignette_name = "Minimal")
```
